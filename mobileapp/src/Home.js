import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity, Button } from 'react-native';
import * as ImagePicker from 'expo-image-picker';
import axios from 'axios';
import * as ImageManipulator from 'expo-image-manipulator';
import * as FileSystem from 'expo-file-system';

export default function Home({ route, navigation }) {
  const [image, setImage] = useState(null);
  const [isProcessing, setProcessing] = useState(false);
  const [predictions, setPredictions] = useState([]);

  useEffect(() => {
    (async () => {
      await ImagePicker.requestMediaLibraryPermissionsAsync();
    })();
  }, []);

  const resizeImage = async (uri) => {
    const manipResult = await ImageManipulator.manipulateAsync(
      uri,
      [{ resize: { width: 800 } }],
      { compress: 0.7, format: ImageManipulator.SaveFormat.JPEG }
    );

    return manipResult.uri;
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3],
      quality: 1,
    });

    if (!result.cancelled) {
      setProcessing(true);

      try {
        const { uri } = result;

        // Resize and compress the image
        const resizedImageUri = await resizeImage(uri);

        // Convert the image to base64
        const base64Image = await FileSystem.readAsStringAsync(resizedImageUri, {
          encoding: FileSystem.EncodingType.Base64,
        });

        // Make a POST request to the API
        const response = await axios.post(
          'https://detect.roboflow.com/waste-segregation-qmxhr/1',
          base64Image,
          {
            params: {
              api_key: 'Re0ER74gwXxoCUpSvO5H',
            },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          }
        );

        // Handle the prediction response
        if (response && response.data) {
          console.log(response.data);
          navigation.replace('Result', { imageurl: uri, predictions: response.data.predictions });
        } else {
          navigation.replace('Result', { imageurl: uri });
        }
      } catch (error) {
        console.log(error);
      } finally {
        setProcessing(false);
      }
    }
  };

  return (
     <View style={styles.container}>
        
        <View style={styles.top}>
            <Text style={styles.header}>LET US PUT OUR WASTE IN RIGHT PLACE</Text>
            <Text style={styles.subHead}>We will help you achieve it!</Text>  
            
            <Image source={require('../assets/waste.png')}  style={{width: 230, height: 150,marginLeft:150, marginTop:-80}} />
        </View>

        <Text style={styles.body}>How to use our app?</Text>
        <View style={styles.box}/> 
        <Text style={styles.body1}>Click on the camera button to open your camera and click image to get the result.</Text>
        <View style={styles.box}/> 
        <Text style={styles.body1}>You can also upload your image by clicking the image button and selecting the image from your gallery and get the result.</Text>
        
        <Image source={require('../assets/line.png')} style={{width: 300, height: 200,margin:45, marginTop:-15,opacity:0.2}}/>
       
        <TouchableOpacity style={styles.but} onPress={()=>{navigation.navigate("Photo")}}>
        <Image source={require('../assets/cemera.png')} style={{height:40,width:70, opacity:0.5}}/>

        </TouchableOpacity>


        <TouchableOpacity style={styles.but1} onPress={pickImage}>
          <Image source={require('../assets/gallary.png')} style={{height:40,width:70,opacity:0.5}}/>
        </TouchableOpacity>
        
        
       
        
       
      </View>
  );
}
const styles = StyleSheet.create({
    container: {
      flex: 1,
     
    },
  
    top: {
      
      backgroundColor: '#0051B0',
      height: 300,
     
    },
    header:{
      color:'white',
      fontSize:30,
      fontWeight:"bold",
      marginTop:100,
      width:330,
      marginLeft:15
    },
    subHead:{
      color:'#D9D9D9',
      fontSize:13,
      marginLeft:15
    },
    body:{
      color:'#313131',
      fontWeight:'500',
      fontSize:18,
      padding:15,
      paddingBottom:0,
      paddingTop:20
  
    },
    body1:{
      color:'grey',
      fontWeight:'400',
      fontSize:16,
      paddingLeft:33,
     marginTop:-17,
      width:380
      
    },
    box:{
      backgroundColor:'#313131',
      height:10,
      width:10,
      borderRadius:100,
      margin:15,
      marginBottom:0,
      opacity:0.7
  
    },
    but:{
      backgroundColor:'#F7F7F7',
      height:110,
      width:110,
      shadowColor:'grey',
      elevation:5,
      justifyContent:'center',
      alignItems: 'center',
      borderRadius:20,
      marginLeft:50,
      marginTop:-40
      
    },
    but1:{
      backgroundColor:'#F7F7F7',
      height:110,
      width:110,
      shadowColor:'grey',
      elevation:5,
      justifyContent:'center',
      alignItems: 'center',
      borderRadius:20,
      marginLeft:230,
      marginTop:-110
      
    }
   
  });