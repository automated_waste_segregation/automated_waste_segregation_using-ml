import { Camera } from 'expo-camera';
import * as MediaLibrary from 'expo-media-library';
import React, { useState, useEffect, useRef } from 'react';
import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import axios from 'axios';
import * as ImageManipulator from 'expo-image-manipulator';
import * as FileSystem from 'expo-file-system';

export default function Photo({ navigation }) {
  const [hasCameraPermission, setHasCameraPermission] = useState(null);
  const [isProcessing, setProcessing] = useState(false);
  const [type, setType] = useState(Camera.Constants.Type.back);
  const cameraRef = useRef(null);
  const [predictions, setPredictions] = useState([]);

  useEffect(() => {
    (async () => {
      await MediaLibrary.requestPermissionsAsync();
      const cameraStatus = await Camera.requestCameraPermissionsAsync();
      setHasCameraPermission(cameraStatus.status === 'granted');
    })();
  }, []);

  const resizeImage = async (uri) => {
    const manipResult = await ImageManipulator.manipulateAsync(
      uri,
      [{ resize: { width: 800 } }],
      { compress: 0.7, format: ImageManipulator.SaveFormat.JPEG }
    );

    return manipResult.uri;
  };

  const takePhoto = async () => {
    if (cameraRef.current && !isProcessing) {
      setProcessing(true);

      try {
        const { uri } = await cameraRef.current.takePictureAsync();

        // Resize and compress the image
        const resizedImageUri = await resizeImage(uri);

        // Convert the image to base64
        const base64Image = await FileSystem.readAsStringAsync(resizedImageUri, {
          encoding: FileSystem.EncodingType.Base64,
        });

        // Make a POST request to the API
        const response = await axios.post(
          'https://detect.roboflow.com/waste-segregation-qmxhr/1',
          base64Image,
          {
            params: {
              api_key: 'Re0ER74gwXxoCUpSvO5H',
            },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          }
        );

        // Handle the prediction response
        if (response && response.data) {
          console.log(response.data);
          navigation.replace('Result', { imageurl: uri, predictions: response.data.predictions });
        } else {
          navigation.replace('Result', { imageurl: uri });
        }}
         catch (error) {
        console.log(error);
      }
       finally {
        setProcessing(false);
      }
    }
  };

  if (hasCameraPermission === false) {
    return <Text>No access to Camera!</Text>;
  }

  return (
    <View style={styles.container}>
      <Camera style={styles.camera} type={type} ref={cameraRef} />

      {predictions.map((prediction, index) => (
        <View
          key={index}
          style={{
            position: 'absolute',
            left: prediction.x,
            top: prediction.y,
            width: prediction.width,
            height: prediction.height,
            borderWidth: 2,
            borderColor: 'red',
          }}
        >
          <Text style={{ color: 'red', fontSize: 16 }}>{prediction.class}</Text>
          <Text style={{ color: 'red', fontSize: 14 }}>Confidence: {prediction.confidence}</Text>
        </View>
      ))}

      <TouchableOpacity
        onPress={takePhoto}
        disabled={isProcessing}
        style={{ marginLeft: 155, marginTop: 750, position: 'absolute' }}
      >
        <View style={styles.Click}>
          <View style={styles.Click1} />
        </View>
      </TouchableOpacity>

      <TouchableOpacity
        style={{ position: 'absolute', marginTop: 50, marginLeft: 20 }}
        onPress={() => {
          navigation.goBack();
        }}
      >
        <Image source={require('../assets/arrow.png')} style={{ height: 20, width: 20 }} />
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
  },
  camera: {
    borderRadius: 20,
    flex: 1,
  },
  Click: {
    height: 80,
    width: 80,
    borderWidth: 2,
    borderColor: 'white',
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Click1: {
    backgroundColor: 'red',
    height: 65,
    width: 65,
    borderRadius: 100,
  },
});
// import { Camera } from 'expo-camera';
// import * as MediaLibrary from 'expo-media-library';
// import React, { useState, useEffect, useRef } from 'react';
// import { StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
// import axios from 'axios';
// import * as ImageManipulator from 'expo-image-manipulator';
// import * as FileSystem from 'expo-file-system';
// import Canvas from 'react-native-canvas';

// export default function Photo({ navigation }) {
//   const [hasCameraPermission, setHasCameraPermission] = useState(null);
//   const [isProcessing, setProcessing] = useState(false);
//   const [type, setType] = useState(Camera.Constants.Type.back);
//   const cameraRef = useRef(null);
//   const [predictions, setPredictions] = useState([]);

//   useEffect(() => {
//     (async () => {
//       await MediaLibrary.requestPermissionsAsync();
//       const cameraStatus = await Camera.requestCameraPermissionsAsync();
//       setHasCameraPermission(cameraStatus.status === 'granted');
//     })();
//   }, []);

//   const resizeImage = async (uri) => {
//     const manipResult = await ImageManipulator.manipulateAsync(
//       uri,
//       [{ resize: { width: 800 } }],
//       { compress: 0.7, format: ImageManipulator.SaveFormat.JPEG }
//     );

//     return manipResult.uri;
//   };

//   const takePhoto = async () => {
//     if (cameraRef.current && !isProcessing) {
//       setProcessing(true);

//       try {
//         const { uri } = await cameraRef.current.takePictureAsync();

//         // Resize and compress the image
//         const resizedImageUri = await resizeImage(uri);

//         // Convert the image to base64
//         const base64Image = await FileSystem.readAsStringAsync(resizedImageUri, {
//           encoding: FileSystem.EncodingType.Base64,
//         });

//         // Make a POST request to the API
//         const response = await axios.post(
//           'https://detect.roboflow.com/waste-segregation-qmxhr/1',
//           base64Image,
//           {
//             params: {
//               api_key: 'Re0ER74gwXxoCUpSvO5H',
//             },
//             headers: {
//               'Content-Type': 'application/x-www-form-urlencoded',
//             },
//           }
//         );

//         // Handle the prediction response
//         if (response && response.data) {
//           console.log(response.data);
//           navigation.replace('Result', { imageurl: uri, predictions: response.data.predictions });
//         } else {
//           navigation.replace('Result', { imageurl: uri });
//         }}
//          catch (error) {
//         console.log(error);
//       }
//        finally {
//         setProcessing(false);
//       }
//     }
//   };

//   if (hasCameraPermission === false) {
//     return <Text>No access to Camera!</Text>;
//   }

//   return (
//     <View style={styles.container}>
//       <Camera style={styles.camera} type={type} ref={cameraRef} />

//       <Canvas style={styles.canvas} ref={(canvas) => {
//         if (canvas) {
//           canvas.width = 800; // Adjust the canvas width as per your image size
//           canvas.height = 600; // Adjust the canvas height as per your image size
//           const context = canvas.getContext('2d');

//           // Draw boundary boxes
//           predictions.forEach((prediction) => {
//             context.beginPath();
//             context.rect(prediction.x, prediction.y, prediction.width, prediction.height);
//             context.lineWidth = 2;
//             context.strokeStyle = 'red';
//             context.fillStyle = 'transparent';
//             context.stroke();
//             context.closePath();

//             // Add text labels
//             context.fillStyle = 'red';
//             context.font = '16px Arial';
//             context.fillText(prediction.class, prediction.x, prediction.y - 5);
//             context.fillText(`Confidence: ${prediction.confidence}`, prediction.x, prediction.y + prediction.height + 18);
//           });
//         }
//       }} />

//       <TouchableOpacity
//         onPress={takePhoto}
//         disabled={isProcessing}
//         style={{ marginLeft: 155, marginTop: 750, position: 'absolute' }}
//       >
//         <View style={styles.Click}>
//           <View style={styles.Click1} />
//         </View>
//       </TouchableOpacity>

//       <TouchableOpacity
//         style={{ position: 'absolute', marginTop: 50, marginLeft: 20 }}
//         onPress={() => {
//           navigation.goBack();
//         }}
//       >
//         <Image source={require('../assets/arrow.png')} style={{ height: 20, width: 20 }} />
//       </TouchableOpacity>
//     </View>
//   );
// }

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: 'black',
//   },
//   camera: {
//     borderRadius: 20,
//     flex: 1,
//   },
//   canvas: {
//     position: 'absolute',
//     left: 0,
//     top: 0,
//   },
//   Click: {
//     height: 80,
//     width: 80,
//     borderWidth: 2,
//     borderColor: 'white',
//     borderRadius: 100,
//     justifyContent: 'center',
//     alignItems: 'center',
//   },
//   Click1: {
//     backgroundColor: 'red',
//     height: 65,
//     width: 65,
//     borderRadius: 100,
//   },
// });
