import { View, Text, Image, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import React, { useState, useEffect } from 'react';
import * as ImagePicker from 'expo-image-picker';
import { ScrollView } from 'react-native';
import * as ImageManipulator from 'expo-image-manipulator';
import axios from 'axios';
import * as FileSystem from 'expo-file-system';
const Result = ({ route, navigation }) => {
  const [image, setImage] = useState(null);
  const { imageurl, predictions } = route.params;
  const [processing, setProcessing] = useState(false);


  const [windowWidth, setWindowWidth] = useState(Dimensions.get('window').width);
  const [windowHeight, setWindowHeight] = useState(Dimensions.get('window').height);

  useEffect(() => {
    const updateDimensions = () => {
      setWindowWidth(Dimensions.get('window').width);
      setWindowHeight(Dimensions.get('window').height);
    };

    Dimensions.addEventListener('change', updateDimensions);

    return () => {
      Dimensions.removeEventListener('change', updateDimensions);
    };
  }, []);

  useEffect(() => {
    (async () => {
      await ImagePicker.requestMediaLibraryPermissionsAsync();
    })();
  }, []);

  const resizeImage = async (uri) => {
    const manipResult = await ImageManipulator.manipulateAsync(
      uri,
      [{ resize: { width: 800 } }],
      { compress: 0.7, format: ImageManipulator.SaveFormat.JPEG }
    );

    return manipResult.uri;
  };

  const pickImage = async () => {
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [4, 3],
      quality: 1,
    });
  
    if (!result.cancelled) {
      try {
        const { uri } = result;
  
        // Resize and compress the image
        const resizedImageUri = await resizeImage(uri);
  
        // Convert the image to base64
        const base64Image = await FileSystem.readAsStringAsync(resizedImageUri, {
          encoding: FileSystem.EncodingType.Base64,
        });
  
        // Make a POST request to the API
        const response = await axios.post(
          'https://detect.roboflow.com/waste-segregation-qmxhr/1',
          base64Image,
          {
            params: {
              api_key: 'Re0ER74gwXxoCUpSvO5H',
            },
            headers: {
              'Content-Type': 'application/x-www-form-urlencoded',
            },
          }
        );
  
        // Handle the prediction response
        if (response && response.data) {
          console.log(response.data);
          navigation.replace('Result', { imageurl: uri, predictions: response.data.predictions });
        } else {
          navigation.replace('Result', { imageurl: uri });
        }
      } catch (error) {
        console.log(error);
      }
    }
  };
  
  const getBoundaryBoxStyle = (prediction) => {
    if (!prediction || !prediction.bbox || !image) {
      return {}; // Return empty object if prediction or bbox is missing
    }
  
    const { x, y, width, height } = prediction.bbox;
  
    const imageAspectRatio = image.width / image.height;
    const containerAspectRatio = windowWidth / windowHeight;
  
    let boxWidth, boxHeight, boxX, boxY;
  
    if (imageAspectRatio > containerAspectRatio) {
      // Image is wider than the container
      boxWidth = windowWidth;
      boxHeight = windowWidth / imageAspectRatio;
      boxX = 0;
      boxY = (windowHeight - boxHeight) / 2;
    } else {
      // Image is taller than the container
      boxWidth = windowHeight * imageAspectRatio;
      boxHeight = windowHeight;
      boxX = (windowWidth - boxWidth) / 2;
      boxY = 0;
    }
  
    const scaledBoxX = boxX + (x / image.width) * boxWidth;
    const scaledBoxY = boxY + (y / image.height) * boxHeight;
    const scaledBoxWidth = (width / image.width) * boxWidth;
    const scaledBoxHeight = (height / image.height) * boxHeight;
  
    return {
      position: 'absolute',
      left: scaledBoxX,
      top: scaledBoxY,
      width: scaledBoxWidth,
      height: scaledBoxHeight,
      borderWidth: 2,
      borderColor: 'red',
    };
  };

  return (
    <View style={styles.container}>
      <View style={styles.header}>
        <TouchableOpacity onPress={() => navigation.replace('Home')}>
          <Image source={require('../assets/arrow.png')} style={styles.backButton} />
        </TouchableOpacity>
        <Text style={styles.title}>Result</Text>
      </View>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View style={styles.imageContainer}>
          {image ? <Image source={{ uri: image.uri }} style={styles.image} /> : <Image source={{ uri: imageurl }} style={styles.image} />}
  
            {predictions &&
            predictions.map((prediction, index) => {
            const boxStyle = getBoundaryBoxStyle(prediction);

            return (
            <React.Fragment key={index}>
            <View style={[styles.boundaryBox, boxStyle]} />
            <Text style={[styles.classText, { left: boxStyle.left, top: boxStyle.top }]}>
            {prediction.class}
            {prediction.confidence}
            </Text>
      </React.Fragment>
    );
  })}
        </View>
        {/* Rest of the code */}
        <TouchableOpacity style={styles.pick} onPress={pickImage}>
          <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Select photo</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.click} onPress={() => navigation.navigate('Photo')}>
          <Text style={{ fontSize: 15, fontWeight: 'bold', color: 'grey' }}>Click photo</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Result;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    backgroundColor: '#0051B0',
    height: 100,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  backButton: {
    height: 20,
    width: 20,
    marginLeft: -170,
    marginBottom: -25,
  },
  title: {
    color: 'white',
    fontSize: 18,
    fontWeight: '400',
    marginBottom: 15,
  },
  scrollView: {
    flexGrow: 1,
  },
  imageContainer: {
    height: 540,
    width: 340,
    borderColor: 'grey',
    borderWidth: 0.5,
    marginTop: 20,
    margin: 25,
  },
  image: {
    flex: 1,
  },
  boundaryBox: {
    borderWidth: 2,
    borderColor: 'red',
    position: 'absolute',
    justifyContent: 'center',
    alignItems: 'center',
  },
  classText: {
    color: 'red',
    fontSize: 16,
  },
  confidenceText: {
    color: 'red',
    fontSize: 14,
  },
  pick: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  click: {
    marginTop: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  
  // Rest of the styles
});
