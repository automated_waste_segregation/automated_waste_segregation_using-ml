import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import Home from './src/Home';
import Result from './src/detail';
import Photo from './src/camera';

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

const Stack = createNativeStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
    <Stack.Navigator
    screenOptions={{
      headerShown: false
    }}>
      <Stack.Screen
        name="Home"
        component={Home}
      />
      <Stack.Screen 
        name="Result" 
        component={Result} />
        <Stack.Screen 
        name="Photo" 
        component={Photo} />
    </Stack.Navigator>
  </NavigationContainer>
      // <Result/>

      // <Photo/>
   
    
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#0051B0',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });
